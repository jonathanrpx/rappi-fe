/**
 * Created by Jonathan Rodriguez on 15/01/2017.
 */

app.service('CharacterService', ['$http', 'UtilsFactory',
    function ($http, UtilsFactory) {
        return {
            findAll: function (offset, limit) {
                var hash = UtilsFactory.hash();
                return $http({
                    url: UtilsFactory.path + '/characters',
                    method: "GET",
                    params: {
                        'apikey': hash.publicKey,
                        'hash': hash.hash,
                        'ts': hash.ts,
                        'offset': offset,
                        'limit': limit
                    }
                });
            }
        }
    }]);