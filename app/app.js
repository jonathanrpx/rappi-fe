var app = angular.module('rappi-fe', ['ui.bootstrap']);

app.constant('apiPath', 'http://gateway.marvel.com/v1/public')
    .constant('apiPublicKey', '431c5e6cf4b25e4905dc6c3f6892d9db')
    .constant('apiPrivateKey', 'ad6c7ddcd3859bd45e279a40f9ac95d8a7cba0f8');

app.factory('UtilsFactory', ['apiPath', 'apiPublicKey', 'apiPrivateKey',
    function (apiPath, apiPublicKey, apiPrivateKey) {
        return {
            hash: function () {
                var ts = new Date().getTime();
                return {
                    publicKey: apiPublicKey,
                    hash: md5.hex(ts + apiPrivateKey + apiPublicKey),
                    ts: ts
                }
            },
            path: apiPath
        };
    }
]);