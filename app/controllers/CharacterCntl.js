/**
 * Created by Jonathan Rodriguez on 15/01/2017.
 */

app.controller('CharacterCntl', ['$scope', 'CharacterService',
    function ($scope, CharacterService) {
        $scope.characters = [];
        $scope.totalCharacters = 0;
        $scope.currentPage = 1;
        $scope.limit = 10;

        $scope.findAll = function () {
            CharacterService
                .findAll(($scope.currentPage - 1) * $scope.limit, $scope.limit)
                .then(function (response) {
                        $scope.characters = response.data.data.results;
                        $scope.totalCharacters = response.data.data.total;
                    },
                    function (error) {

                    });
        };
    }]);